﻿using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.DTOs;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parking;

        public VehiclesController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<VehicleReadDTO>), StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<VehicleReadDTO>> GetVehicles()
        {
            return Ok(_parking.GetVehicles().Select(vehicle => vehicle.ToReadDTO()));
        }

        [HttpGet("{id}", Name = nameof(GetVehicle))]
        [ProducesResponseType(typeof(VehicleReadDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<VehicleReadDTO> GetVehicle([FromRoute]string id)
        {
            if (!Vehicle.ValidateId(id))
                return BadRequest();

            var vehicle = _parking.GetVehicles().FirstOrDefault(x => x.Id == id);

            if (vehicle == null)
                return NotFound();

            return Ok(vehicle.ToReadDTO());
        }

        [HttpPost]
        [ProducesResponseType(typeof(VehicleReadDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<VehicleReadDTO> AddVehicle([FromBody] VehicleCreateDTO vehicleDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var vehicle = vehicleDto.ToVehicle();

            _parking.AddVehicle(vehicle);
            return CreatedAtRoute(nameof(GetVehicle), new { Id = vehicle.Id }, vehicle.ToReadDTO());
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult RemoveVehicle([FromRoute] string id)
        {
            if (!Vehicle.ValidateId(id))
                return BadRequest();

            var vehicle = _parking.GetVehicles().FirstOrDefault(x => x.Id != id);

            if (vehicle == null)
                return NotFound();

            if (vehicle.Balance <= 0)
                return BadRequest("Cannot remove vehicle with negative balance");

            _parking.RemoveVehicle(id);

            return NoContent();
        }
    }
}
