﻿using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.DTOs;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parking;

        public TransactionsController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet("last")]
        [ProducesResponseType(typeof(IEnumerable<TransactionInfo>), StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            return Ok(_parking.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public ActionResult<string> GetAllTransactions()
        {
            return Ok(_parking.ReadFromLog());
        }

        [HttpPut("topUpVehicle")]
        [ProducesResponseType(typeof(Vehicle), StatusCodes.Status200OK)]
        public ActionResult<VehicleReadDTO> TopUpVehicle([FromBody]TopUpDTO topUpDto)
        {
            if (!Vehicle.ValidateId(topUpDto.Id))
                return BadRequest();

            var vehicle = _parking.GetVehicles().FirstOrDefault(x => x.Id == topUpDto.Id);

            if (vehicle == null)
                return NotFound();

            _parking.TopUpVehicle(topUpDto.Id, topUpDto.Sum);

            return Ok(vehicle.ToReadDTO());
        }
    }
}
