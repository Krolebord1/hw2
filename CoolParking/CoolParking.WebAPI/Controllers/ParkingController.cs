﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/parking")]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parking;

        public ParkingController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet("balance")]
        [ProducesResponseType(typeof(decimal), StatusCodes.Status200OK)]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parking.GetBalance());
        }

        [HttpGet("capacity")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parking.GetCapacity());
        }

        [HttpGet("freePlaces")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parking.GetFreePlaces());
        }
    }
}
