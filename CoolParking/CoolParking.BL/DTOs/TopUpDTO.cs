﻿namespace CoolParking.BL.DTOs
{
    public class TopUpDTO
    {
        public string Id { get; }

        public decimal Sum { get; }

        public TopUpDTO(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }
    }
}
