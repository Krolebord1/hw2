﻿using CoolParking.BL.Models;

namespace CoolParking.BL.DTOs
{
    public class VehicleCreateDTO
    {
        public string Id { get; set; }

        public VehicleType VehicleType { get; set; }

        public decimal Balance { get; set; }

        public VehicleCreateDTO(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
    }

    public static class VehicleCreateDTOMapper
    {
        public static Vehicle ToVehicle(this VehicleCreateDTO createDto) =>
            new (createDto.Id, createDto.VehicleType, createDto.Balance);
    }
}
