﻿using CoolParking.BL.Models;

namespace CoolParking.BL.DTOs
{
    public class VehicleReadDTO
    {
        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; }

        public VehicleReadDTO(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
    }

    public static class VehicleReadDTOMapper
    {
        public static VehicleReadDTO ToReadDTO(this Vehicle vehicle) =>
            new (vehicle.Id, vehicle.VehicleType, vehicle.Balance);
    }
}
