﻿using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialBalance = 0;

        public const int ParkingCapacity = 10;

        public const int ChargePeriod = 5;

        public const int LogPeriod = 60;

        public const decimal FineMultiplier = 2.5m;

        public static decimal GetRate(VehicleType type) =>
            type switch
            {
                VehicleType.PassengerCar => 2,
                VehicleType.Truck => 5,
                VehicleType.Bus => 3.5m,
                VehicleType.Motorcycle => 1,
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
    }
}
