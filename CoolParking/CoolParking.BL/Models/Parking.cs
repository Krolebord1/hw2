﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }

        public int Capacity { get; }

        public List<Vehicle> Vehicles { get; }

        public int FreePlaces => Capacity - Vehicles.Count;

        public Parking(decimal balance, int capacity)
        {
            Balance = balance;

            Capacity = capacity;

            Vehicles = new List<Vehicle>(Capacity);
        }
    }
}
