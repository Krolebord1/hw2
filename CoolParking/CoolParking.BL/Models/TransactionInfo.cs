﻿using System;
using System.Globalization;
using System.Text;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; }

        public decimal Sum { get; }

        public DateTime TransactionDate { get; }

        public TransactionInfo(string vehicleId, decimal sum, DateTime date)
        {
            VehicleId = vehicleId;
            Sum = sum;
            TransactionDate = date;
        }

        public override string ToString() =>
            $"{TransactionDate.ToLongDateString()} {TransactionDate.ToLongTimeString()}: " +
            $"{Sum} money withdrawn from vehicle with Id='{VehicleId}'";
    }
}
