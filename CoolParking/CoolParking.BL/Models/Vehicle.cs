﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if(!ValidateId(id))
                throw new ArgumentException(null, nameof(id));

            if(balance < 0)
                throw new ArgumentException(null, nameof(balance));

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static bool ValidateId(string id)
        {
            var idRegex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");

            return idRegex.IsMatch(id);
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rng = new Random();

            char[] chars = new char[4];

            for (int i = 0; i < 4; i++)
            {
                int num = rng.Next(0, 26);
                chars[i] = (char)('A' + num);
            }

            return $"{chars[0]}{chars[1]}-{rng.Next(1000, 10000)}-{chars[2]}{chars[3]}";
        }
    }
}
