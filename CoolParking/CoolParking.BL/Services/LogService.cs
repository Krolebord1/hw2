﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public void Write(string logInfo)
        {
            using StreamWriter log = File.AppendText(LogPath);
            log.WriteLine(logInfo);
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException();

            using StreamReader log = File.OpenText(LogPath);
            return log.ReadToEnd();
        }
    }
}
