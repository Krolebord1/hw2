﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Start()
        {
            _timer = new Timer(Interval);

            _timer.Elapsed += Elapsed;

            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Elapsed -= Elapsed;
            _timer.Dispose();
        }
    }
}
