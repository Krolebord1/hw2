﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private static Lazy<Parking> lazyParking =
            new(() => new Parking(0, Settings.ParkingCapacity));

        private static Parking Parking =>
            lazyParking.Value;

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        private readonly List<TransactionInfo> _transactions;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += Withdraw;
            _withdrawTimer.Start();

            _transactions = new List<TransactionInfo>();

            _logTimer.Elapsed += LogTransactions;
            _logTimer.Start();
        }

        public void Dispose()
        {
            _withdrawTimer.Stop();
            _withdrawTimer.Dispose();

            _logTimer.Stop();
            _logTimer.Dispose();

            lazyParking = new Lazy<Parking>(() => new Parking(0, Settings.ParkingCapacity));
        }

        public decimal GetBalance() =>
            Parking.Balance;

        public int GetCapacity() =>
            Parking.Capacity;

        public int GetFreePlaces() =>
            Parking.FreePlaces;

        public ReadOnlyCollection<Vehicle> GetVehicles() =>
            new ReadOnlyCollection<Vehicle>(Parking.Vehicles);

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() <= 0)
                throw new InvalidOperationException();

            if (Parking.Vehicles.Any(x => x.Id == vehicle.Id))
                throw new ArgumentException();

            Parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = Parking.Vehicles.Find(x => x.Id == vehicleId);

            if(vehicle == null)
                throw new ArgumentException();

            if (vehicle.Balance <= 0)
                throw new InvalidOperationException();

            Parking.Vehicles.RemoveAll(x => x.Id == vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException();

            var vehicle = Parking.Vehicles.Find(x => x.Id == vehicleId);

            if(vehicle == null)
                throw new ArgumentException();

            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions() =>
            _transactions.ToArray();

        public string ReadFromLog() =>
            _logService.Read();

        private void Withdraw(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var vehicle in Parking.Vehicles)
                _transactions.Add(WithdrawFromVehicle(vehicle));
        }


        private TransactionInfo WithdrawFromVehicle(Vehicle vehicle)
        {
            decimal withdrawAmount = Settings.GetRate(vehicle.VehicleType);

            if (vehicle.Balance <= 0)
                withdrawAmount *= Settings.FineMultiplier;
            else if (vehicle.Balance < withdrawAmount)
                withdrawAmount = vehicle.Balance + (withdrawAmount - vehicle.Balance) * Settings.FineMultiplier;

            vehicle.Balance -= withdrawAmount;
            Parking.Balance += withdrawAmount;

            return new TransactionInfo(vehicle.Id, withdrawAmount, DateTime.Now);
        }

        private void LogTransactions(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            _logService.Write("");
            foreach (var transaction in _transactions)
                _logService.Write(transaction.ToString());

            _transactions.Clear();
        }
    }
}
