﻿
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp
{
    static class Program
    {
        static void Main(string[] args)
        {
            IProgram app = new ConsoleApp();

            app.Run();
        }
    }
}
