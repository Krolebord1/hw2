﻿namespace CoolParking.ConsoleApp.Interfaces
{
    public interface IProgram
    {
        public void Run();
    }
}
