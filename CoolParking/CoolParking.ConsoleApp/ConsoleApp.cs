﻿using System.Collections.Generic;
using CoolParking.ConsoleApp.Interfaces;
using CoolParking.ConsoleApp.Programs;

namespace CoolParking.ConsoleApp
{
    public class ConsoleApp : IProgram
    {
        public void Run()
        {
            var navigator = new Navigator(new List<KeyValuePair<string, IProgram>>()
            {
                new ("Get balance", new GetBalanceProgram()),
                new ("Get capacity", new GetCapacityProgram()),
                new ("Get free places", new GetFreePlacesProgram()),

                new ("Get last transactions", new GetLastTransactionsProgram()),
                new ("Get transactions history", new GetHistoryProgram()),
                new ("Top up vehicle balance", new TopUpProgram()),

                new ("Get vehicles", new GetVehiclesProgram()),
                new ("Get vehicle", new GetVehicleProgram()),
                new ("Add vehicle", new AddVehicleProgram()),
                new ("Remove vehicle", new RemoveVehicleProgram())

            });

            navigator.Run();
        }
    }
}
