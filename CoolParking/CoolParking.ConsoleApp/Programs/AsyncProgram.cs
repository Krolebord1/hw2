﻿using System;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public abstract class AsyncProgram : ProgramBase
    {
        public override void Run()
        {
            Console.Clear();

            RunAsync().Wait();
        }

        protected abstract Task RunAsync();
    }
}
