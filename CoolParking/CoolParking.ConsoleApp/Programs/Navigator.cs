﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.Programs
{
    public class Navigator : IProgram
    {
        private List<NavigatorEntry> _entries;

        public Navigator(IEnumerable<KeyValuePair<string, IProgram>> programs)
        {
            _entries = programs
                .Select((kvp, index) => new NavigatorEntry(index, kvp.Key, kvp.Value))
                .ToList();
        }

        public void Run()
        {
            while(true)
            {
                Console.Clear();
                WriteCatalog();

                string args = Console.ReadLine()?.ToLower();

                if(string.IsNullOrWhiteSpace(args))
                    break;

                NavigatorEntry entry = int.TryParse(args, out int index)
                    ? _entries.Find(x => x.index == index)
                    : _entries.Find(x => x.path.ToLower() == args.ToLower());

                if (entry != null)
                {
                    Console.Title = entry.path;
                    entry.program.Run();
                    Console.Title = "App";
                }
            }
        }

        private void WriteCatalog()
        {
            Console.WriteLine("Select action: ");
            foreach (NavigatorEntry entry in _entries)
                Console.WriteLine($"{entry.index}. {entry.path}");

            Console.WriteLine("Or press enter to go back.");
        }

        private class NavigatorEntry
        {
            public readonly int index;
            public readonly string path;
            public readonly IProgram program;

            public NavigatorEntry(int index, string path, IProgram program)
            {
                this.index = index;
                this.path = path;
                this.program = program;
            }
        }
    }
}
