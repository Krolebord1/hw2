﻿using System;

namespace CoolParking.ConsoleApp.Programs
{
    public class TextProgram : ProgramBase
    {
        private Func<string> text;

        public TextProgram(Func<string> text)
        {
            this.text = text;
        }

        public override void Run()
        {
            Console.Clear();
            Console.WriteLine(text());
            WaitForKey();
        }
    }
}
