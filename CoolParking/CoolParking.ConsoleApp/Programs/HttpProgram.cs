﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public abstract class HttpProgram : AsyncProgram
    {
        protected override async Task RunAsync()
        {
            try
            {
                await RunWithClientAsync(ParkingClient.Client);
            }
            catch(HttpRequestException e)
            {
                PrintError(e.Message);
            }
        }

        protected abstract Task RunWithClientAsync(HttpClient client);
    }
}
