﻿using System;
using CoolParking.BL.Models;
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.Programs
{
    public abstract class ProgramBase : IProgram
    {
        public abstract void Run();

        protected string GetVehicleId()
        {
            Console.WriteLine("Enter vehicle id (XX-YYYY-XX):");
            string vehicleId = Console.ReadLine();

            if (Vehicle.ValidateId(vehicleId))
                return vehicleId;

            PrintError("Invalid vehicle id");
            return null;
        }

        protected VehicleType? GetVehicleType()
        {
            Console.WriteLine("Select vehicle type:");

            var types = Enum.GetValues<VehicleType>();

            for (int i = 0; i < types.Length; i++)
                Console.WriteLine($"\t{i}. {Enum.GetName(types[i])}");

            var val = Console.ReadLine();

            if (int.TryParse(val, out var type) && type >= 0 && type < types.Length)
                return types[type];

            PrintError("Invalid type");
            return null;
        }

        protected decimal? GetSum()
        {
            Console.WriteLine("Enter sum:");
            var val = Console.ReadLine();

            if (decimal.TryParse(val, out var sum))
                return sum;

            PrintError("Invalid sum");
            return null;
        }

        protected void WaitForKey()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        protected void PrintError(string message = default)
        {
            Console.WriteLine();
            Console.WriteLine("Error occurred");

            if(!string.IsNullOrWhiteSpace(message))
                Console.WriteLine($"Message: \n{message}");

            WaitForKey();
        }
    }
}
