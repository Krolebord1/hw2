﻿using System;

namespace CoolParking.ConsoleApp.Programs
{
    public class ActionProgram : ProgramBase
    {
        private Action action;

        public ActionProgram(Action action)
        {
            this.action = action;
        }

        public override void Run()
        {
            Console.Clear();

            action();

            WaitForKey();
        }
    }
}
