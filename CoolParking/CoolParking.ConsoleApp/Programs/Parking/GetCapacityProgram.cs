﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetCapacityProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var response = await ParkingClient.Client.GetAsync(ApiUrls.Parking.Capacity());

            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            if (int.TryParse(responseBody, out int capacity))
            {
                Console.WriteLine($"Capacity: {capacity.ToString()}");
                WaitForKey();
            }
            else PrintError();
        }
    }
}
