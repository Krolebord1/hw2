﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetLastTransactionsProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var response = await ParkingClient.Client.GetAsync(ApiUrls.Transactions.Last());

            response.EnsureSuccessStatusCode();

            var transactions = await response.Content.ReadFromJsonAsync<IEnumerable<TransactionInfo>>();

            if (transactions == null)
            {
                WaitForKey();
                return;
            }

            Console.WriteLine("Transactions:");
            foreach (TransactionInfo transactionInfo in transactions)
            {
                Console.WriteLine(transactionInfo.ToString());
            }

            WaitForKey();
        }
    }
}
