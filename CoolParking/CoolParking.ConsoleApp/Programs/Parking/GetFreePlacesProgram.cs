﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetFreePlacesProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var response = await ParkingClient.Client.GetAsync(ApiUrls.Parking.FreePlaces());

            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            if (int.TryParse(responseBody, out int freePlaces))
            {
                Console.WriteLine($"Free places: {freePlaces.ToString()}");
                WaitForKey();
            }
            else PrintError();
        }
    }
}
