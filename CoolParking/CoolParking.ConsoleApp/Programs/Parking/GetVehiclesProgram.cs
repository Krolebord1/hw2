﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CoolParking.BL.DTOs;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetVehiclesProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var response = await client.GetAsync(ApiUrls.Vehicles.GetAll());

            response.EnsureSuccessStatusCode();

            var vehicles = await response.Content.ReadFromJsonAsync<IEnumerable<VehicleReadDTO>>();

            Console.WriteLine("Vehicles:");

            if (vehicles == null)
            {
                WaitForKey();
                return;
            }

            foreach (var vehicle in vehicles)
            {
                Console.WriteLine("\tId: " + vehicle.Id);
                Console.WriteLine("\tType: " + vehicle.VehicleType);
                Console.WriteLine("\tBalance: " + vehicle.Balance);
                Console.WriteLine();
            }

            WaitForKey();
        }
    }
}
