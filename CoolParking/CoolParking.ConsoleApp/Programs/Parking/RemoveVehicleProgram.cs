﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public class RemoveVehicleProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            string vehicleId = GetVehicleId();
            if(string.IsNullOrEmpty(vehicleId)) return;

            var response = await client.DeleteAsync(ApiUrls.Vehicles.Delete(vehicleId));

            response.EnsureSuccessStatusCode();

            Console.WriteLine("Vehicle has been removed");

            WaitForKey();
        }
    }
}
