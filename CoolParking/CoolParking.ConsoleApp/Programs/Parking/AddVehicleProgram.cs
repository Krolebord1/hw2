﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CoolParking.BL.DTOs;

namespace CoolParking.ConsoleApp.Programs
{
    public class AddVehicleProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            string vehicleId = GetVehicleId();
            if(string.IsNullOrEmpty(vehicleId)) return;

            decimal? balance = GetSum();
            if(balance == null) return;

            var type = GetVehicleType();
            if(type == null) return;

            var vehicle = new VehicleCreateDTO(vehicleId, type.Value, balance.Value);

            var response = await client.PostAsJsonAsync(ApiUrls.Vehicles.Create(), vehicle);

            response.EnsureSuccessStatusCode();

            Console.WriteLine();
            Console.WriteLine("Vehicle has been added");

            WaitForKey();
        }
    }
}
