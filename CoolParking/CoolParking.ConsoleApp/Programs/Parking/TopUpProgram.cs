﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CoolParking.BL.DTOs;

namespace CoolParking.ConsoleApp.Programs
{
    public class TopUpProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var vehicleId = GetVehicleId();

            if (string.IsNullOrEmpty(vehicleId)) return;

            decimal? sum = GetSum();
            if(sum == null) return;

            TopUpDTO topUpDTO = new TopUpDTO(vehicleId, sum.Value);

            var response = await client.PutAsJsonAsync(ApiUrls.Transactions.TopUpVehicle(), topUpDTO);

            response.EnsureSuccessStatusCode();

            Console.WriteLine();
            Console.WriteLine("Operation succeeded");

            WaitForKey();
        }
    }
}
