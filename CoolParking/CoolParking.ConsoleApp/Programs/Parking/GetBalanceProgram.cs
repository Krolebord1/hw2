﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetBalanceProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var response = await ParkingClient.Client.GetAsync(ApiUrls.Parking.Balance());

            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            if (decimal.TryParse(responseBody, out decimal balance))
            {
                Console.WriteLine($"Balance: {balance.ToString()}");
                WaitForKey();
            }
            else PrintError();
        }
    }
}
