﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetHistoryProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            var response = await ParkingClient.Client.GetAsync(ApiUrls.Transactions.All());

            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            Console.WriteLine("History:");
            Console.WriteLine(responseBody);

            WaitForKey();
        }
    }
}
