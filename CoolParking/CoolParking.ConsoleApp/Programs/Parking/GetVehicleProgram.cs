﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CoolParking.BL.DTOs;

namespace CoolParking.ConsoleApp.Programs
{
    public class GetVehicleProgram : HttpProgram
    {
        protected override async Task RunWithClientAsync(HttpClient client)
        {
            string vehicleId = GetVehicleId();
            if(string.IsNullOrEmpty(vehicleId)) return;

            var response = await client.GetAsync(ApiUrls.Vehicles.Get(vehicleId));

            response.EnsureSuccessStatusCode();

            var vehicle = await response.Content.ReadFromJsonAsync<VehicleReadDTO>();

            if (vehicle == null)
            {
                PrintError();
                return;
            }

            Console.WriteLine();
            Console.WriteLine("Vehicle:");
            Console.WriteLine("\tId: " + vehicle.Id);
            Console.WriteLine("\tType: " + vehicle.VehicleType);
            Console.WriteLine("\tBalance: " + vehicle.Balance);
            Console.WriteLine();

            WaitForKey();
        }
    }
}
