﻿namespace CoolParking.ConsoleApp
{
    public static class ApiUrls
    {
        public static readonly string Scheme = "https";
        public static readonly string Authority = "localhost:5001";
        public static readonly string ApiPath = "api";

        public static readonly string FullApiPath = $"{Scheme}://{Authority}/{ApiPath}";

        public static class Parking
        {
            public static string Balance() => $"{FullApiPath}/parking/balance";
            public static string Capacity() => $"{FullApiPath}/parking/capacity";
            public static string FreePlaces() => $"{FullApiPath}/parking/freePlaces";
        }

        public static class Transactions
        {
            public static string Last() => $"{FullApiPath}/transactions/last";
            public static string All() => $"{FullApiPath}/transactions/all";
            public static string TopUpVehicle() => $"{FullApiPath}/transactions/topUpVehicle";
        }

        public static class Vehicles
        {
            public static string GetAll() => $"{FullApiPath}/vehicles";
            public static string Get(string vehicleId) => $"{FullApiPath}/vehicles/{vehicleId}";
            public static string Create() => $"{FullApiPath}/vehicles";
            public static string Delete(string vehicleId) => $"{FullApiPath}/vehicles/{vehicleId}";
        }
    }
}
